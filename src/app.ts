import * as express from "express";
import * as handlebars from "handlebars";
import * as fs from "fs";
import * as nconf from "nconf";
import * as helmet from "helmet";
import * as compression from "compression";

const prod = "PROD";

const log = (txt: string) => console.log(txt);

nconf.file("settings.json");

let env = nconf.get("env");

log(`Config read, env: ${env}`);

let app = express();
let portNum: Number = nconf.get("port");

log(`Config read, port: ${portNum}`);

/* pre-compile the main page */
let html: string;
fs.readFile("templates/main.handlebars", "utf-8", function(error, source) {
  if(error) {
    console.error("Could not compile main page! Abort ship!");
    return false;
  }
  var template = handlebars.compile(source);
  html = template({});
  return true;
});

app.get(
  "/",
  (_req, res, next) => {
    res.send(html);
    next();
  }
);

// environment-specific code
if(env == prod) {

  // rely on nginx for ssl, but still use helmet for HSTS and compression
  // to get all the headers
  app
    .use(helmet.hsts({
      maxAge: 31536000000,
      includeSubdomains: true,
      force: true
    }))
    .use(compression);
  

} else {
  // TODO do stuff for dev... live reload? Other fancy crap?
}

console.log(`App listening on port ${portNum}`);
app.listen(portNum);

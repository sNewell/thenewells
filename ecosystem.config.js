module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [

    // First application
    {
      name      : 'Newells App',
      script    : 'src/app.js',
      instances: 1, // TODO not sure how this will work with two express apps listening on same port...?
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production : {
        NODE_ENV: 'production'
      }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : 'newellsweb',
      host : 'thenewells.us',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:sNewell/thenewells',
      path : '/srv/thenewells',
      'post-deploy' : 'pwd && npm install && tsc && cp ../prod-settings.json settings.json && pm2 reload ecosystem.config.js --env production'
    }
  }
};

The Newells
======

### Personal website for The Newell family.


Currently, we are developing this project actively for multiple purposes.

- To Learn more web development (both for Sean&Nicki)
- To self-host Nicki's online art gallery
- To host both Nicki and Sean's blogs
- To experiment with new and old design techniques
- To have a web presence to back up the notion that we are web designers



Active tasks:

1. Use express with nodejs for the main landing page
2. Use ghost to power the two blogs
3. Create a 1-click publish set up
4. Use a free SSL solution
